//
//  ViewController.h
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDNetworkExchange.h"

@interface ViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSArray *countries;
@property (nonatomic, strong) NSArray *results;

@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UITextField *convertFromSelector;
@property (weak, nonatomic) IBOutlet UITextField *convertToSelector;

@property (nonatomic, strong) MDNetworkExchange *exchangeHandler;

@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;

@end

