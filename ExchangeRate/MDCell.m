//
//  MDCell.m
//  ExchangeRate
//
//  Created by Martin Doyle on 03/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import "MDCell.h"

@implementation MDCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
