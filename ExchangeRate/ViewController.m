//
//  ViewController.m
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import "ViewController.h"
#import "MDTableViewController.h"


@interface ViewController ()

@property (nonatomic, strong) UIPickerView *toPicker;
@property (nonatomic, strong) UIPickerView *fromPicker;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _countries = [self.exchangeHandler getExchangeRates];
    _convertToSelector.inputView = self.toPicker;
    _convertFromSelector.inputView = self.fromPicker;
    _results = [[NSArray alloc] init];
}

-(MDNetworkExchange *)exchangeHandler{
    if (!_exchangeHandler) {
        _exchangeHandler = [[MDNetworkExchange alloc] init];
    }
    return _exchangeHandler;
}

-(UIPickerView *)toPicker{
    if (!_toPicker) {
        _toPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,280, 320, 100)];
        _toPicker.delegate = self;
        _toPicker.dataSource = self;
        _toPicker.backgroundColor = [UIColor lightGrayColor];
        _toPicker.showsSelectionIndicator = YES;
        _toPicker.tag = 1;
        UIToolbar *mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
        
        mypickerToolbar.barStyle = UIBarStyleBlackOpaque;
        
        [mypickerToolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
        [barItems addObject:doneBtn];
        [mypickerToolbar setItems:barItems animated:YES];
        _convertToSelector.inputAccessoryView = mypickerToolbar;

    }
    return _toPicker;
    
}

-(UIPickerView *)fromPicker{
    if (!_fromPicker) {
        
        _fromPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,280, 320, 100)];
        _fromPicker.delegate = self;
        _fromPicker.dataSource = self;
        _fromPicker.backgroundColor = [UIColor lightGrayColor];
        _fromPicker.showsSelectionIndicator = YES;
        _fromPicker.tag = 0;
        
        UIToolbar *mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
        mypickerToolbar.barStyle = UIBarStyleBlackOpaque;
        [mypickerToolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
        [barItems addObject:doneBtn];
        [mypickerToolbar setItems:barItems animated:YES];
        _convertFromSelector.inputAccessoryView = mypickerToolbar;
        
    }
    return _fromPicker;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)pickerDoneClicked
{
    [_convertFromSelector resignFirstResponder];
    [_convertToSelector resignFirstResponder];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
                       titleForRow:(NSInteger)row
            forComponent:(NSInteger)component{
    
    if (row == [_countries count]) {
        return @"All";
    }
     MDCountry *country = [_countries objectAtIndex:row];

    return country.countryName;

}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component{
    if (row == [_countries count]) {
        if (pickerView.tag == 0) {
            _convertFromSelector.text = @"All";
        } else {
            _convertToSelector.text = @"All";
        }
        return;
    }
    
    MDCountry *country = [_countries objectAtIndex:row];
    if (pickerView.tag == 0) {
        _convertFromSelector.text = country.countryName;
    } else {
        _convertToSelector.text = country.countryName;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    
    return [_countries count] + 1;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)performConversion:(id)sender {
    
    _results = [_exchangeHandler getExchangeRateForCountry:_convertFromSelector.text toCountry:_convertToSelector.text forAmount:[_inputTextField.text floatValue] ];
    
    MDTableViewController *vc = [[MDTableViewController alloc] init];
    vc.results = _results;
    [self.navigationController pushViewController:vc animated:YES];
    
    NSLog(@"");
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([[segue identifier] isEqualToString:@"showResults"])
    {
        MDTableViewController *vc = [segue destinationViewController];

        [vc setResults:_results];
    }
}




















@end
