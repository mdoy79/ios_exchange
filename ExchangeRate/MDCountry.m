//
//  MDCountry.m
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import "MDCountry.h"

@implementation MDCountry

-(void)addExchangeRate:(NSNumber *)rate forCountry:(NSString *)name{
    
    NSMutableDictionary *newExchangeRate = [[NSMutableDictionary alloc] init];
    [newExchangeRate setObject:rate forKey:@"rate"];
    [newExchangeRate setObject:name forKey:@"name"];
    [self.exchangeRates addObject: newExchangeRate];
    
}

-(NSMutableArray *)exchangeRates{
    if (!_exchangeRates) {
        _exchangeRates = [[NSMutableArray alloc] init];
    }
    return _exchangeRates;
}

-(NSArray *)getExchangeRateForCountry:(NSString *)country forAmout:(CGFloat)amount{
    CGFloat exchangeRate;
    NSMutableArray *resultsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *rate in self.exchangeRates){
        NSString *name = [rate objectForKey:@"name"];
        
        if ([country isEqualToString:@"All"]) {
            exchangeRate = [[rate objectForKey:@"rate"] floatValue];
            NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
            [result setObject:[NSNumber numberWithFloat:exchangeRate] forKey:@"rate"];
            
            CGFloat calculatedResult = amount * exchangeRate;
            
            NSString *resultString = [NSString stringWithFormat:@"%.2f %@ = %.2f %@", amount, self.countryName, calculatedResult, name];
            [result setObject:resultString forKey:@"resultString"];
            [resultsArray addObject:result];

        } else
        {
            if ([[rate objectForKey:@"name"] isEqualToString:country]) {
                exchangeRate = [[rate objectForKey:@"rate"] floatValue];
                NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
                [result setObject:[NSNumber numberWithFloat:exchangeRate] forKey:@"rate"];
                
                CGFloat calculatedResult = amount * exchangeRate;
                
                NSString *resultString = [NSString stringWithFormat:@"%.2f %@ = %.2f %@", amount, self.countryName, calculatedResult, country];
                [result setObject:resultString forKey:@"resultString"];
                
                NSArray *resultArray = [NSArray arrayWithObject:result];
                return resultArray;
            }
        }
        
        
    }
    return resultsArray;
}


@end
