//
//  MDNetworkExchange.h
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MDCountry.h"

@interface MDNetworkExchange : NSObject

@property (nonatomic, strong) NSMutableDictionary *exchangeRates;
@property (nonatomic, strong) NSMutableArray *countries;
@property (nonatomic, strong) NSMutableArray *countryList;

-(NSArray *)getExchangeRates;
-(NSArray *)getExchangeRateForCountry:(NSString *)fromCountry toCountry:(NSString *)toCountry forAmount:(CGFloat )amount;
-(MDCountry *)getCountry:(NSString *)name;
-(BOOL)countryExists: (NSString *)name;


@end
