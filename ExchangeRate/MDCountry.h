//
//  MDCountry.h
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MDCountry : NSObject

@property (nonatomic, strong) NSMutableArray *exchangeRates;
@property (nonatomic, strong) UIImageView *flag;
@property (nonatomic, strong) NSString *countryName;

-(void)addExchangeRate:(NSNumber *)rate forCountry:(NSString *)name;
-(NSArray *)getExchangeRateForCountry:(NSString *)country forAmout:(CGFloat)amount;

@end
