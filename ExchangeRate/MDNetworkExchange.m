//
//  MDNetworkExchange.m
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import "MDNetworkExchange.h"
static NSString * const baseURL = @"https://raw.githubusercontent.com/mydrive/code-tests/master/iOS-currency-exchange-rates/rates.json";

@implementation MDNetworkExchange

-(instancetype)init{
    self = [super init];
    _countries = [[NSMutableArray alloc] init];
    _countryList = [[NSMutableArray alloc] init];
    return self;
}


-(NSArray *)getExchangeRates{
    NSData *exchangeRateData = [[NSData alloc] initWithContentsOfURL:
                              [NSURL URLWithString:baseURL]];
    NSError *error;
    _exchangeRates = [NSJSONSerialization
                                       JSONObjectWithData:exchangeRateData
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    
    if( error )
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {

        [self processExchangeRates];
    }
    return [NSArray arrayWithArray:_countries];
}

-(BOOL)countryExists: (NSString *)name{
    
    BOOL countryExists = NO;
    for (MDCountry *country in _countries) {
        
        if ([country.countryName isEqualToString:name]) {
            countryExists = YES;
            return countryExists;
        }
    }
    return countryExists;
}
-(MDCountry *)getCountry:(NSString *)name{
    for (MDCountry *country in _countries) {
        
        if ([country.countryName isEqualToString:name]) {
            return country;
        }
    }
    return nil;
}

- (void)processCountry:(NSString *)countryName withRate:(NSNumber *)rate toCountry:(NSString *)toCountry{
    if ([self countryExists:countryName]) {
        MDCountry *country = [self getCountry:countryName];
        [country addExchangeRate:rate forCountry:toCountry];
    } else {
        // new country never seen before
        MDCountry *country = [[MDCountry alloc] init];
        country.countryName = countryName;
        [country addExchangeRate:rate forCountry:toCountry];
        [_countries addObject:country];
    }
}

-(NSArray *)getExchangeRateForCountry:(NSString *)fromCountry toCountry:(NSString *)toCountry forAmount:(CGFloat )amount{
    
    NSArray *resultsArray = [[NSArray alloc] init];
    if (![fromCountry isEqualToString:@"All"]) {
        MDCountry *from = [self getCountry:fromCountry];
        resultsArray = [from getExchangeRateForCountry:toCountry forAmout:amount];
        return resultsArray;
    } else {
        NSMutableArray *combinedResults = [[NSMutableArray alloc] init];
        for (MDCountry *country in _countries) {
            resultsArray = [country getExchangeRateForCountry:toCountry forAmout:amount];
            [combinedResults addObjectsFromArray:resultsArray];
        }
        return combinedResults;
    }
   
}

-(void)calculateAllExchangeRates{
    
    MDCountry *usd = [self getCountry:@"USD"];
    NSMutableArray *usdExchangeValues = [NSMutableArray arrayWithArray:usd.exchangeRates];
    
    NSArray *countriesCopy = [NSArray arrayWithArray:_countries];
    
    
    for (MDCountry *country in countriesCopy) {
        NSMutableArray* newArray = [[NSMutableArray alloc] init];
        if (![country.countryName isEqualToString:@"USD"]) {
            
            NSArray *result = [self getExchangeRateForCountry:country.countryName toCountry:@"USD" forAmount:0];
            NSDictionary *dict = [result objectAtIndex:0];
            CGFloat usdExchange = [[dict objectForKey:@"rate"] floatValue];
           

            
            for (NSMutableDictionary *dict in usdExchangeValues) {
                
                CGFloat rate = [[dict objectForKey:@"rate"] floatValue];
                CGFloat combinedRate = rate *usdExchange;
                
                
                NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
                [newDictionary setObject:[NSNumber numberWithFloat:combinedRate] forKey:@"rate"];
                [newDictionary setObject:[dict objectForKey:@"name"] forKey:@"name"];
                [newArray addObject:newDictionary];
                
            }
            NSMutableDictionary *usdRate = [[NSMutableDictionary alloc] init];
            [usdRate setObject:@"USD" forKey:@"name"];
            [usdRate setObject:[NSNumber numberWithFloat: usdExchange] forKey:@"rate"];
            [newArray addObject:usdRate];
        
        country.exchangeRates = [NSMutableArray arrayWithArray:newArray];
        }
    }

      NSLog(@"");
}

-(void)processExchangeRates{
    
    NSArray *rates = _exchangeRates[@"conversions"];
    for ( NSDictionary *item in rates )
    {
        NSString *rateString = item[@"rate"];
        NSString *fromCountry = item[@"from"];
        NSString *toCountry = item[@"to"];
        NSNumber *rate = [NSNumber numberWithFloat:[rateString floatValue]];
        CGFloat inverseRateFloat = 1 / [rateString floatValue];
        NSNumber *inverseRate = [NSNumber numberWithFloat:inverseRateFloat];
        
        [self processCountry:fromCountry withRate:rate toCountry: toCountry];
        [self processCountry:toCountry withRate:inverseRate toCountry:fromCountry];
        // create a list of country names supported
           }
    for (MDCountry *country in _countries) {
        [_countryList addObject:country.countryName];
    }
    [self calculateAllExchangeRates];

    
    NSLog(@"");
}
    


@end
