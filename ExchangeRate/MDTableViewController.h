//
//  MDTableViewController.h
//  ExchangeRate
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *results;

@end
