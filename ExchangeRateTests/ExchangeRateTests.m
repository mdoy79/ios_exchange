//
//  ExchangeRateTests.m
//  ExchangeRateTests
//
//  Created by Martin Doyle on 02/08/2015.
//  Copyright (c) 2015 Martin Doyle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MDNetworkExchange.h"
#import "MDCountry.h"

@interface ExchangeRateTests : XCTestCase

@property (nonatomic, strong) MDNetworkExchange *networkExchange;

@end

@implementation ExchangeRateTests

- (void)setUp {
    [super setUp];
    _networkExchange = [[MDNetworkExchange alloc] init];
    [_networkExchange getExchangeRates];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testGetCountryReturnsNilForAFalseCountry{
    
    MDCountry *testCountry = [_networkExchange getCountry:@"Fake Country"];
    
    XCTAssertNil(testCountry);
    
}

-(void)testCountryExistsReturnsTrueForARealCountry{
    BOOL countryExists = [_networkExchange countryExists:@"EUR"];
    XCTAssertTrue(countryExists);
    
}

-(void)testCountryExistsReturnsNoForAFalseCountry{
    
    BOOL countryExists = [_networkExchange countryExists:@"Tatooine"];
    XCTAssertFalse(countryExists);
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
